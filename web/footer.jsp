<%-- 
    Document   : footer
    Created on : 03-nov-2016, 16:24:43
    Author     : omauri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <hr>
        <footer class="footer">
            <div class="container">
                <p class="text-center text-muted">2016 © Oriol Mauri Guiu </p>
            </div>
          </footer>
    
    </body>
</html>
