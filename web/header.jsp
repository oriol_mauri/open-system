<%-- 
    Document   : header
    Created on : 03-nov-2016, 16:23:25
    Author     : omauri
--%>

<%@page import="cat.urv.deim.sob.entities.users.UserVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--https://www.bootstrapcdn.com/-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        
    </head>
    <%
        UserVO user = (UserVO) session.getAttribute("user");
    %>
    <body>
    <div id="navbar" class="navbar navbar-default">
            <ul class="nav navbar-nav">
                
              <li class="active"><a name="action"  href='init.do'> Inici </a></li>

              <li class="active"><a name="action"  href='list_offers.do'> Totes les Ofertes </a></li>
              
              
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <%
                  if (session.getAttribute("user") != null){
                      out.print("<li class='active' ><a href='list_reserves.do'>Les meves Reserves</a></li>");
                       String name = user.getFirstName();
                      out.print("<li class='active'><a href='profile.do'>Benvingut "+name+" !!<!--img class='media-object' src='static/img/profile.png' alt='profile' style='width: 10px; height: 10px;' data-holder-rendered='true'--></a> </li>");
                  }else{
                      out.print("<li class='active'><a href='login.do'>Iniciar Sessió</a></li>");
                  }
                %>
            </ul>
          </div>
    </body>
</html>
