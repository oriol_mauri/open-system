<%-- 
    Document   : list
    Created on : 12-oct-2016, 20:22:30
    Author     : omauri
--%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.util.*, cat.urv.deim.sob.entities.offers.*, cat.urv.deim.sob.dao.*"%>
<html>
	<head>
		<title>Offer Details</title>
	</head>
<body>
    <%
        OfferVO vo = (OfferVO) request.getAttribute("offer");
    %>
    <div class="container">


      <div class="jumbotron">
        <h1 class="display-3"><%= vo.getName() %></h1>
        <p class="lead"><%= vo.getShortDescription() %></p>
      </div>

      <div class="row marketing">
        <div class="col-lg-12">
          <h4>Què és?</h4>
          <p> <%= vo.getLongDescription() %> </p>

          <h4>Foto</h4>
            <img class="media-object" src="static/img/offer_<%= vo.getIdOffer() %>.png" alt="offer" style="width: 200px; height: 200px;" data-holder-rendered="true">
        </div>
          <div class="col-lg-6">
              <h4>Estrelles</h4>
              <p> <%= vo.getStars() %> </p>
              
              <h4>Preu</h4>
              <p> <%= vo.getCostPerPeople() %> €/persona</p>
              
          </div>
          <div class="col-lg-6">
              <h4>Vots</h4>
              <p> <%= vo.getVotes() %> </p>
          </div>
          <%
            if (vo.getRooms() <= 0){
                out.print("<p>No hi ha places disponibles!.</p>");
            }else{
                out.print("<form method='post' action='reserve_offer.do'>"
                            +"<table>"
                            +" <p name=id_offer value='"+ vo.getIdOffer() +"' ></p>"
                            +"    "
                            +"    <tr>"
                            +"        <td>"
                            +"            <input type='hidden' name='id_offer' value='"+ vo.getIdOffer() +"' readonly>"
                            +"        </td>"
                            +"    </tr>"
                        
                            +"     <tr>"
                            +"        <th>"
                            +"            Número de reserves"
                            +"        </th>"
                            +"        <td>"
                            +"            <input type='text' name='rooms'><p>" +vo.getRooms()+ "- Places disponibles</p>"
                            +"        </td>"
                            +"    </tr>"
                            +"    <tr>"
                            +"        <th></th>"
                            +"        <td>"
                            +"            <button class='btn btn-success' type='submit'>Reservar Offerta</p></button>"
                            +"        </td>"
                            +"    </tr>"
                            +"</table>"
                          +"</form>");
            }
        %>
      </div>
    </div>
</body>
</html>

<%@ include file="../../footer.jsp" %>
