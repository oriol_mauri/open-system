<%-- 
    Document   : list
    Created on : 12-oct-2016, 20:22:10
    Author     : omauri
--%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.util.*, cat.urv.deim.sob.entities.offers.*, cat.urv.deim.sob.dao.*"%>

<html>
    <head>
        <title>Offers list</title>
    </head>
<body>
    <h1>Llistat d'Ofertes</h1>

    <% 
        Iterator it = (Iterator) request.getAttribute("offer_list");        
    %>
    <ul class="media-list">
        <!--init_for-->
<%       while (it.hasNext()) {
	OfferVO vo = (OfferVO)it.next();
%>
    <div class="container jumbotron">
        <li class="media">
          <div class="media-left">
              <a href="details_offer.do?id_offer=<%= vo.getIdOffer() %>">
            <img class="media-object" src="static/img/offer_<%out.print(vo.getIdOffer());%>.png" alt="offer" style="width: 64px; height: 64px;" data-holder-rendered="true">
            </a>
          </div>
          <div class="media-body">
              <a href="details_offer.do?id_offer=<%= vo.getIdOffer() %>">
                <h4 class="media-heading"> <%= vo.getName() %> </h4>
              </a>
            <p><%= vo.getShortDescription() %> </p>
        </li>
    </div>
<%
}
%>

        <!--end_for-->
      </ul>
</body>
</html>

<%@ include file="../../footer.jsp" %>