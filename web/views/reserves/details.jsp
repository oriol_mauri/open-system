<%-- 
    Document   : detail
    Created on : 03-nov-2016, 15:42:26
    Author     : omauri
--%>

<%@page import="cat.urv.deim.sob.entities.reserve.ReserveVO"%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reserve Detail</title>
    </head>
    <body>
        <h1>Reserve Detail</h1>
    <%
        ReserveVO vo = (ReserveVO) request.getAttribute("reserve");
    %>
    <div class="container">

      <div class="jumbotron">
        <h1 class="display-3"> <%= vo.getName() %> </h1>
        <p class="lead"> <%= vo.getShortDescription() %> </p>
      </div>

      <div class="row marketing">
        <div class="col-lg-12">
          <h4>Què és?</h4>
          <p> <%= vo.getLongDescription() %> </p>

          <h4>Foto</h4>
            <img class="media-object" src="static/img/offer_<%= vo.getIdOffer() %>.png" alt="offer" style="width: 64px; height: 64px;" data-holder-rendered="true">        
        </div>
        <div class="col-lg-6">
            <h4>Estrelles</h4>
            <p> <%= vo.getStars() %></p>

            <h4>Preu</h4>
            <p> <%= vo.getCostPerPeople() %> €/persona</p>
          </div>
          <div class="col-lg-6">
              <h4>Vots</h4>
              <p> <%= vo.getVotes() %> </p>
              
              <h4>Número de reserves</h4>
              <p> <%= vo.getNum() %> persona</p>
          </div>
        <div class="col-lg-12">
              <h4>Total</h4>
              <%
                  float total = vo.getNum()*vo.getCostPerPeople();
              %>
              <p> <%= total %> €</p>        
        </div>

      </div>
              <p><a class='btn btn-lg btn-warning' href='delete_reserve.do?id_reserve=<%= vo.getIdReserve() %>' role='button'>Cancelar Reserva</a></p>
    </div>
    </body>
</html>

<%@ include file="../../footer.jsp" %>