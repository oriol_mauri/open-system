<%-- 
    Document   : list
    Created on : 03-nov-2016, 15:42:12
    Author     : omauri
--%>
<%@page import="cat.urv.deim.sob.entities.reserve.ReserveVO"%>
<%@page import="java.util.Iterator"%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reserves List</title>
    </head>
    <body>
        <h1>Llista de Reserves</h1>
    <% 
        int i = 1; 
        Iterator it = (Iterator) request.getAttribute("reserves_list");
    %>
    
    <ul class="media-list">
        <!--init_for-->
    <%       
        while (it.hasNext()) {
	ReserveVO vo = (ReserveVO)it.next();
    %>
    <div class="container jumbotron">
            
        <li class="media">
          <div class="media-left">
              <a href="details_reserve.do?id_reserve=<%= vo.getIdReserve() %> ">
            <img class="media-object" src="static/img/offer_<%= vo.getIdOffer() %>.png" alt="offer" style="width: 64px; height: 64px;" data-holder-rendered="true">
            </a>
          </div>
          <div class="media-body">
            <a href="details_reserve.do?id_reserve=<%= vo.getIdReserve() %> ">
                <h4 class="media-heading"> <%= vo.getName() %> </h4>
            </a>
            <p> <%= vo.getShortDescription() %> </p>
          </div>
        </li>
    </div>
<%
}
%>

        <!--end_for-->
      </ul>    
    </body>
</html>

<%@ include file="../../footer.jsp" %>
