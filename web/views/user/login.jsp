<%-- 
    Document   : login
    Created on : 12-oct-2016, 19:21:05
    Author     : omauri
--%>

<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container" align="center">
            <div class="row">
                <div class="container" id="formContainer">

                  <form method="post" action="login_submit.do">
                    <h3 class="form-signin-heading">Iniciar Sessió</h3>
                    <a href="login.do" id="flipToRecover" class="flipLink">
                      <div id="triangle-topright"></div>
                    </a>
                    <table>
                        <tr>
                            <th>
                                Email
                            </th>
                            <td>
                                <input type="email" class="form-control" name="email" id="loginEmail" placeholder="Email address" required autofocus>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Contrasenya
                            </th>                            
                            <td>
                                <input type="password" class="form-control" name="password" id="loginPass" placeholder="Password" required>
                            </td>
                        </tr>
                        <br>
                        <tr>
                            <td>
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                            </td>
                        </tr>
                    </table>      
                  </form>

                  <!--form class="form-signin" id="recover" role="form"-->
                    <h2 class="form-signin-heading">No tens compte?</h2>
                    <p><a class='btn btn-lg btn-success' href='register.do' role='button'>Registra't</a></p>
                <!--/form-->

                </div> <!-- /container -->
            </div>
        </div>
    </body>
</html>

<%@ include file="../../footer.jsp" %>