<%-- 
    Document   : list
    Created on : 12-oct-2016, 20:22:10
    Author     : omauri
--%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.util.*, cat.urv.deim.sob.entities.users.*, cat.urv.deim.sob.dao.*"%>
<html>
	<head>
		<title>Profile</title>
	</head>
<body>
    <%
        user = (UserVO) request.getSession().getAttribute("user");
    %>
<div class="container" align="center">
            <div class="row">
                <div class="container" id="formContainer">

                  <form method="post" action="update_user.do" id="profile">
                    <h3 class="form-signin-heading">Perfil</h3>
                    <table>
                        <tr>
                            <th>
                                Email
                            </th>
                            <td>
                                <input type="email" name="email" value="<%= user.getEmail() %>" readonly>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Contrasenya !
                            </th>
                            <td>
                                <input type="password" name="password" id="password" value="<%= user.getPassword() %>" required>
                                <input type="checkbox" onchange="document.getElementById('password').type = this.checked ? 'text' : 'password'"> Mostrar
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Nom
                            </th>
                            <td>
                                <input type="text" name="first_name" value="<%= user.getFirstName()%>" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Cognom
                            </th>
                            <td>
                                <input type="text" name="last_name" value="<%= user.getLastName() %>" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Telèfon
                            </th>
                            <td>
                                <input type="text" name="phone" value="<%= user.getPhone() %>">
                            </td>
                        </tr>
                    </table>      
                  </form>
                <button class="btn btn-success" type="submit" form="profile">Actualitzar</button>
                <br>
                <!--TODO: Comprobar quin botó funciona:-->
                <br>
                    <p><a class='btn btn-lg btn-warning' href='logout_submit.do' role='button'>Tancar Sessió</a></p>
                    <!--p><a class='btn btn-lg btn-danger' href='delete_user.do' role='button'>Esborrar compte</a></p-->
                    </div>
                <!-- /container -->
            </div>
        </div>
</body>
</html>

<%@ include file="../../footer.jsp" %>