<%--
    Document   : register
    Created on : 03-nov-2016, 15:41:12
    Author     : omauri
--%>
<%@ include file="../../header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        <%
            String email = "";
            if (request.getAttribute("email") != null){
                email = (String) request.getAttribute("email");
            }
        %>
        <div class="container" align="center">
            <div class="row">
                <h1>Formulari de Registre</h1>
                <div class="container" id="formContainer">

                  <form method="post" action="register_submit.do">
                    <h3 class="form-signin-heading">Perfil</h3>
                    <table>
                        <tr>
                            <th>
                                Email
                            </th>
                            <td>
                                <input type="email" name="email" value="<%= email%> " required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Nom
                            </th>
                            <td>
                                <input type="text" name="first_name" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Cognom
                            </th>
                            <td>
                                <input type="text" name="last_name" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Telèfon
                            </th>
                            <td>
                                <input type="text" name="phone" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Contrasenya !
                            </th>
                            <td>
                                <input type="password" name="password" id="password" required>
                                <input type="checkbox" onchange="document.getElementById('password').type = this.checked ? 'text' : 'password'"> Mostrar
                            </td>
                        </tr>
                        <br>
                        <tr>
                            <th></th>
                            <td>
                                <button class="btn btn-success" type="submit">Registrar-se</button>
                            </td>
                        </tr>
                    </table>      
                  </form>
                    <h3>Ja tens compte?</h3>
                    <p><a class='btn btn-lg btn-info' href='login.do' role='button'>Iniciar Sessió</a></p>
                </div> <!-- /container -->
            </div>
        </div>
    </body>
</html>

<%@ include file="../../footer.jsp" %>