<%-- 
    Document   : index
    Created on : 03-nov-2016, 16:47:07
    Author     : omauri
--%>
<%@ include file="header.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <title>index</title>
    </head>
    <body>

        <div class="jumbotron" align="center">
        <h1>SOB Practica de les ofertes</h1>
        <h2>Benvingut a la web de les ofertes!</h2>
        <p>Reserva productes o serveis que es poden adquirir i reservar pel seu gaudi posterior</p>
        <p >
            <%
                if (session.getAttribute("user") == null){
                    out.println("<a class='btn btn-lg btn-primary' href='login.do' role='button'>Iniciar Sessió</a>");
                    out.println("<a class='btn btn-lg btn-primary' href='register.do' role='button'>Registrar-se</a>");
                }
              %>
          <a class="btn btn-lg btn-info" href="list_offers.do" role="button">Veure Ofertes</a>
        </p>
      </div>        
    </body>
</html>
<%@ include file="footer.jsp" %>