


<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<html>
    <head>
        <title>Error page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        
        <div class="jumbotron" align="center">
        <h1>Opps !</h1>
        <h2>Hi ha hagut un error inesperat</h2>
        <h1 >${pageContext.errorData.statusCode}</h1>
        <p >
            <button class="btn btn-lg btn-info" onclick="history.back()">Go back</button>
        </p>
      </div>  
        

        
        
    </body>
</html>

<%@ include file="footer.jsp" %>