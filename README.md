# Pràctica 1 de Sistemes Oberts
 ** Version 1.0 **

### Autor ###

** Oriol Mauri Guiu ** ***(Grup 8)***

- Bitbucket: [oriol_mauri](https://bitbucket.org/oriol_mauri/)
- GitHub: [@maguri](https://github.com/maguri)

### Informació bàsica ###

** MVC ** ( *** Java Web app *** )

- Server: [Glass Fish](https://glassfish.java.net)
- Using: [Servlets](http://docs.oracle.com/javaee/6/tutorial/doc/bnafe.html), [jsp](http://docs.oracle.com/javaee/5/tutorial/doc/bnagx.html), [Java DB](https://docs.oracle.com/javase/tutorial/jdbc/basics/), [HTML](http://www.w3schools.com/html/), [JavaScript](http://www.w3schools.com/js/default.asp), [CSS](http://www.w3schools.com/css/default.asp)
- Editor: [NetBeans](https://netbeans.org/)

### Manual d'instal·lació de l'aplicació web ###

** Mode Zip **

- Descarregar el arxiu comprimit
- Importar-lo a NetBeans

** Mode Git **

- Demanar permís al administrador: [oriol_mauri](https://bitbucket.org/oriol_mauri/)
- Clonar el repositori:
`$ git clone https://username@bitbucket.org/oriol_mauri/sob.git`
- Obrir el projecte amb el NeatBeans

** Pasos Comuns **

Un cop tenim el projecte al NetBeans:

- Compilem el projecte (si és necessari)
- Fem `RUN` al botó
- El Servidor ens obrirà el servei al port 8080 i ja podrem disfrutar de la nostra web al:
[`localhost:8080/SOBASE/`](localhost:8080/SOBASE/)
- Després cal inicialitzar la Base de Dades (Veure Informe de la pràctica)

### Informe de la pràctica ###

##### Pagina principal: #####

La pàgina principal és una pàgina de benvinguda on l'usuari pot escollir entre, Registrar-se, Iniciar Sessió o entrar a la web com a invitat i passar directament a veure el llistat d'offertes

![principal.png](https://bitbucket.org/repo/yrqEoz/images/2505757424-principal.png)

##### Pagina de login: #####

A la pàgina de Iniciar Sessió L'Úsuari haurà de introduir el seu email i la seva contrasenya per verificar el compte. La contrasenya i l'email són requerits per a enviar el formulari, i el email a més té l'`autofocus`.

![login.png](https://bitbucket.org/repo/yrqEoz/images/3360008635-login.png)

- Primer mirem si l'usuari ja ha iniciat sessió, i `si` ho està, el redirigim a la seva pàgina del perfil.
- Per Verificar el compte mirem si existeix el email
- `si no` existeix redirigirem al usuari a la pàgina de registre
- `si` l' email existeix mirem si la contrasenya coincideix
- `si no` coincideix, enviem un missatge d'error a la mateixa pàgina de login
- `si` tot és correcte, redirigim a l'usuari a la seva pàgina del perfil

##### Contrasenya: #####

Encriptada?
> No

Es guarda a la base de dades?
> Sí

Es pot recuperar?
> No

##### Veure detalls de la oferta:#####

Desde la llista d'ofertes podem veure els detalls d'aquesta a partir del paràmetre `?id_offer` que passem pel `GET`. En aquesta vista de detalls de l'oferta l'usuari la podrà reservar si està logejat, si no ho està se'l redireccionarà a la pàgina de login per que ho pugui fer.

##### Reservar oferta: #####

En la vista dels detalls de l'oferta l'usuari la podrà reservar si està logejat, si no ho està se'l redireccionarà a la pàgina de login per que ho pugui fer. El formulari que realitza aquesta acció conté com a paràmetres el id de la oferta que es vol reservar i que no es pot modificar per l'usuari, i el número de places que vol reservar. Si les places que vol reservar són més grans que lesque hi ha disponibles el sistema no li deix fer.

![reservar.png](https://bitbucket.org/repo/yrqEoz/images/182431778-reservar.png)

Què passa si no estàs logejat?

> Et redirigeix a la pàgina de Iniciar Sessió

Què passa si no estàs registrat?

> Et redirigeix a la pàgina per Registrar-te amb el email que has intentat iniciar sessió predefinit en el formulari de registre

##### Disseny:

[![Bootstrap Build Status](https://img.shields.io/travis/MaxCDN/bootstrap-cdn/develop.svg?label=Bootstrap%20Build%20Status&style=flat-square)](https://travis-ci.org/MaxCDN/bootstrap-cdn)

S'ha importat la llibreria d' estil [Bootsrap.css](https://www.bootstrapcdn.com) utilitzant així alguns botons, el navbar i l'estil de tots els elements bàsics per fer més atractiva la navegació per la web.

##### Codi font: #####

Es segueix una estructura **MVC** amb un únic controlador `ServletController` que redirigeix les peticions (ja siguin `GET` o `POST`) als Commands corresponents. Aquests Commands amb l'ajuda dels `daos` necessaris instanciats en l'àmbit aplicació `getContext().setAttribute("dao", dao);` ens donen accés a la Base de Dades, accedint, consultant, guardant, actualitzant i esborrant les dades corresponents.

##### Base de Dades: #####

##### Accés a la Base de Dades: #####

La **Base de Dades** s'inicialitza amb el fitxer `install.jsp`:
> `localhost:8080/SOBASE/install.jsp`

L'accés a la Base de Dades es realitzarà amb l'ajuda dels Daos corresponents a cada objecte (`User`, `Offers` i `Reserves`).

### LICENSE ###

MIT License Copyright (c) 2016 Oriol Mauri Guiu [Universistat Rovira i Virgili](http://www.urv.cat/)

# Pràctica 2 #

> Links d'interès:

moodle:
- [link 1](https://javaeenotes.blogspot.com.es/2011/02/rest-using-jax-rs.html)
- [link 2](https://netbeans.org/kb/docs/websvc/jax-ws.html)
- [link 3](https://netbeans.org/kb/docs/websvc/rest.html)

altres:
- [link 4](http://www.madbit.org/blog/programming/472/restful-web-services-with-j2ee-6/)
- [link 5](http://www.vogella.com/tutorials/REST/article.html#rest)

> Enunciat:

```
Objectius

Extendre la pràctica 1 per afegir-li noves funcionalitats, principalment una API REST per poder consumir els web services d’aquesta aplicació de forma remota.

Enunciat

Partint de la vostra pràctica actual (pràctica 1) es tracta que l'amplieu per tal d'incorporar les funcionalitats següents:

    Definirem API REST comuna per tothom i que els grups hauran d’implementar a les seves pràctiques.

    Actualitzar la implementació del servidor d'ofertes promocionals perquè es doni suport a aquesta nova API REST. Proposem dues alternatives:

        O bé que es pugui configurar a nivell de sistema la URL del servidor REST, per així reusar la mateixa lògica de negoci des d'un client REST i del propi servidor web. En altres paraules, la lògica de negoci que teniu als Commands s'hauria de traslladar al servei REST, i els Commands passarien a ser els clients de l'API REST.

         O bé deixar la web de la pràctica 1 intacta i afegir-hi el servei web a banda.

    Cal implementar els serveis REST per a que treballin i retornin JSON. Eines que us poden ser d'interès: [Gson](https://github.com/google/gson), [JAXB](http://www.oracle.com/technetwork/articles/javase/index-140168.html) ([exemple d'ús](https://examples.javacodegeeks.com/core-java/xml/bind/jaxb-json-example/))

    Fer servir versionat per la sostenibilitat de l’API.

    Cal implementar un client REST per a poder interaccionar i testejar la vostra pràctica. No esteu obligats a fer servir cap tecnologia per implementar el serveis o clients REST, és més, l’essència d’aquest sistema és la simplicitat, i això serà el que més es valorarà de la vostra solució. Suggerència: https://www.getpostman.com/


API

Els serveis web exposats relatius als dos recursos principals (ofertes i usuaris) hauran de seguir la següent especificació. Tots els serveis web són obligatoris d'implementar, a excepció d'allò explícitament indicat a cadascun d'ells.

Per ofertes promocionals:

    GET /rest/api/v1/promos

        Llistat JSON de totes les ofertes promocionals.

    GET /rest/api/v1/promos/${id}

        Retorna una oferta promocional.

    POST /rest/api/v1/promos

        Opcional: afegeix una oferta promocional a partir de les dades JSON.

    PUT /rest/api/v1/promos/${id}

        Opcional: actualitza una oferta promocional a partir de les dade JSON.

    DELETE /rest/api/v1/promos/${id}

        Opcional: elimina una oferta promocional.

    POST /rest/api/v1/promos/${id}/booking

        Reserva el nombre de places indicades per JSON per l'usuari autenticat.
        Requereix que l'usuari estigui autenticat.

Per usuaris:

    GET /rest/api/v1/users

        Opcional: Llistat JSON dels usuaris.

    GET /rest/api/v1/users/${id}

        Retorna la informació personal i el llistat d'ofertes reservades, indicant també quantes places s'han reservat, per l'usuari indicat i que està autenticat.
        Requereix que l'usuari estigui autenticat.

    POST /rest/api/v1/users

        Genera un nou usuari al sistema amb les dades JSON indicades.

    PUT /rest/api/v1/users/${id}

        Opcional: actualitza les dades del perfil de l'usuari autenticat. Si a l'aplicació web es permet editar el perfil de l'usuari, aquest servei web és obligatori.
        Requereix que l'usuari estigui autenticat.

    DELETE /rest/api/v1/users/${id}

        Opcional: elimina del sistema el compte de l'usuari autenticat.
        Requereix que l'usuari estigui autenticat.


Autenticació d'usuari

Per aquells serveis web que requereixen que l'usuari estigui autenticat, heu de cercar un mecanisme per permetre-ho.

L'opció més simple i que per la pràctica seria suficient és passar a cada petició de servei web, com a informació JSON annexa, dades que identifiquin a l'usuari que està realitzant la petició (com ara l'usuari i contrasenya del propi usuari).

Hi ha formes més professionals de realitzar aquesta autenticació i autorització d'operacions REST via JAX-RS. Vegeu-ne un exemple.


Millores

Sempre i quant la part obligatòria estigui ben dissenyada i desenvolupada, amb els següents punts s’aconseguirà pujar nota:

Àmbit actuació REST:

    Permetre que els serveis REST rebin i retornin XML com a l’alternativa al JSON.

    Cuinar codis i missatges HTTP per la gestió de les excepcions remotes (ex. 400).

    Configurar URL de servidor REST d’una pràctica d’un altre grup.

    Ampliar l’API REST per incorporar les parts opcionals indicades.

Àmbit actuació J2EE:

    [Investigar i utilitzar filtres J2EE](http://docs.oracle.com/javaee/6/tutorial/doc/bnagb.html) per simplificar l’autentificació de tota l’aplicació. Podeu considerar l'aplicació de filtres pel que és la part d'aplicació web i la part de serveis web REST.

Àmbit actuació AJAX:

    Des del llistat d'ofertes, quan es fa click a una oferta es mostri l'oferta sencera en un html per sobre del llistat d'ofertes. Vegeu-ne un [exemple](https://codepen.io/naren-irain/post/simple-jquery-css-popup). Igualment, ha de mostrar una opció per veure l'oferta completa en una pàgina sencera (tal i com teníeu fins ara).

    Fer que la reserva d'una oferta promocional funcioni per AJAX, de forma que vegem la confirmació o l'error de la reserva sense recarregar la plana o portar-nos a una de nova.
    Fer que l'acció de "Login" des de qualsevol part de la web no provoqui recarregar la plana web. Per això, es pot mostrar un popup com de l'exemple anterior, i actualitzar l'HTML per passar de "Login" a "Benvingut Marc!" amb els enllaços actualitzats. Això vol dir que una vegada actualitzat i veient el "Benvingut Marc!", si hi faig click hi he de veure les meves reserves realitzades juntament amb les meves dades personals.


Documentació

Juntament amb el codi font caldrà lliurar un informe en format PDF amb la següent estructura:  

    Introducció. Presentació general de la pràctica i dels objectius.  

    Estructura de la pràctica. Per exemple, caldrà especificar l’estructura de projecte, dels fitxers i de la base de dades, entre d’altres.  

    Decisions de disseny. Quines alternatives heu considerat a l’hora de fer el projecte i perquè heu escollit l’alternativa escollida enfront de les altres. Aquí, entre altres coses, hauríeu d’explicar quin model dels vistos a classe heu fet servir, quines parts opcionals o millores addicionals heu fet i com heu decidit implementar-les. Heu emprat eines i recursos externs?  

    Jocs de proves realitzats. Quines proves heu fet per assegurar-vos que el vostre projecte funciona correctament en tots els casos possibles. Heu aplicat TDD i/o BDD?  

    Conclusions. A quines conclusions heu arribat en finalitzar aquest projecte. Què heu après, creieu que us pot ser útil en el futur? Llisteu també tres elements de l’assignatura aplicats en aquesta pràctica que us han agradat més, i uns altres tres que us han agradat menys. Els elements estaran ordenats per prioritat, de forma que el primer element en cada llista serà el que més t’hagi agradat més i menys, respectivament.  

    Manual d’instal·lació. La instal·lació s'ha de reduir als següents passos:
    1. Obrir projecte NetBeans,
    2. Donar-li a deploy de la vostra pràctica,
    3. Visitar /install.jsp i
    4. Recarregar el home de la vostra pràctica.

    A més, ha d'haver un usuari de prova anomenat sob i contrasenya sob. Si necessiteu forçosament alguna altra acció, expliciteu-la adequadament.


Entrega del projecte

El projecte s’entregarà via Moodle abans de la data màxima d’entrega que consti allà, la qual està prevista que sigui el 12 de gener de 2017 a les 22h.

Cal que entregueu un arxiu .zip anomenat SOB_P2_nom1_nom2.zip, on nom1 i nom2 seran el nom (i cognoms) de cada un dels components del grup que entrega la pràctica.

    Aquest fitxer zip ha de contenir un fitxer anomenat documentacio.pdf amb la documentació de la pràctica que segueixi el format explicat en el punt anterior.

    A més a més, hi ha d’haver una carpeta anomenada projecte on hi haurà tots els fitxers que formen el projecte en NetBeans, de manera que es pugui obrir directament en el mateix entorn del laboratori.

    Cal deixar ben clar l’ús de l’install.jsp i altres passos que siguin necessaris per posar en funcionament la vostra pràctica.

Avaluació

Cal fer com a mínim les parts obligatòries que us permetran aconseguir fins a un 8.5 de la nota del projecte.

Si feu 3 ampliacions opcionals, una d’elles com a mínim de tipus AJAX, la nota que es podrà aconseguir serà fins a un 10 per aquesta pràctica. Es preferible tenir 3 ampliacions ben desenvolupades que una quantitat superior i que estiguin incorrectes.

Alguns dels aspectes que es valoraran són:  

    Funcionalitat correcte del projecte  

    Estructuració de la pràctica

    Decisions de disseny preses  

    Elegància de la solució proposada a nivell de programació  

    Aspecte visual mitjançant CSS i usabilitat  

    Seguretat i mantenibilitat de l’aplicatiu  

    Compliment dels estàndards i convencions a l’hora de desenvolupar una aplicació web en Java+Netbeans (Java Code Conventions).  

    Qualitat de la documentació presentada.

A una data posterior al lliurament, hi haurà una entrevista de grup amb avaluació individual.

Darrera modificació: divendres, 4 novembre 2016, 18:04

```
