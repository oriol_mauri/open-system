/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.users;

import cat.urv.deim.sob.dao.*;
import java.sql.*;
import java.util.Collection;
import java.util.*;


/**
 *
 * @author omauri
 */
public class UserDaoJavaBase implements IDao{

    final String namedb;
    private static UserDaoJavaBase instance = null;
    
    private UserDaoJavaBase() {
        this.namedb = "sobasedb";
    }
    
    public static UserDaoJavaBase getInstance(){
        if(UserDaoJavaBase.instance == null){
            UserDaoJavaBase.instance = new UserDaoJavaBase();
        }
        return UserDaoJavaBase.instance;
    }
    
    public int VerifyPassword(String email, String password) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".users WHERE email = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (password.equals(rs.getString("password"))){
                    return 0;   //existeix i pssw OK
                }
                return 1; // existeix i psswd BAD
            }
        return 2;   // no existeix usuari
        
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    
    public IValueObject findByEmail(String email) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".users WHERE email = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new UserVO (rs.getInt("id"),
                        rs.getString("first_name"),
                        rs.getString("second_name"),
                        rs.getString("email"),
                        rs.getString("phone"),
                        rs.getString("password"));
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * Not Used!
     * @return
     * @throws DaoException 
     */
    @Override
    public Collection findAll() throws DaoException {
        Collection retorn = new LinkedList();
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".users";
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                retorn.add(new UserVO (rs.getInt("id"),
                        rs.getString("first_name"),
                        rs.getString("second_name"),
                        rs.getString("email"),
                        rs.getString("phone"),
                        rs.getString("password")));
            }
            return retorn;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * Register action
     * @param vo
     * @throws DaoException 
     */
    @Override
    public void add(IValueObject vo) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            // ya existeix Usuari amb aquest email?
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".users WHERE email = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, ((UserVO) vo).getEmail());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {    
                ps.close();
            }else{
        
                sql = "INSERT INTO " +namedb+".users(first_name, second_name, email, phone, password) VALUES (?, ?, ?, ?, ?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, ((UserVO) vo).getFirstName());
                ps.setString(2, ((UserVO) vo).getLastName());
                ps.setString(3, ((UserVO) vo).getEmail());
                ps.setString(4, ((UserVO) vo).getPhone());
                ps.setString(5, ((UserVO) vo).getPassword());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * [OPTIONAL]
     * @param vo
     * @throws DaoException 
     */
    @Override
    public void update(IValueObject vo) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "UPDATE " +namedb+".users SET first_name=?,second_name=?,phone=?,password=? WHERE email=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, ((UserVO) vo).getFirstName());
            ps.setString(2, ((UserVO) vo).getLastName());
            ps.setString(3, ((UserVO) vo).getPhone());
            ps.setString(4, ((UserVO) vo).getPassword());
            ps.setString(5, ((UserVO) vo).getEmail());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * Remove account!
     * @param key
     * @throws DaoException 
     */
    @Override
    public void delete(IKey key) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "DELETE FROM " +namedb+".users WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, ((UserPk) key).getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    private Connection createConnection() throws Exception {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection("jdbc:derby://localhost:1527/sobasedb", "oriol", "oriol");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Override
    public IValueObject findByPrimaryKey(IKey key) throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
