package cat.urv.deim.sob.entities.users;

import cat.urv.deim.sob.dao.IKey;
import cat.urv.deim.sob.dao.IValueObject;
import cat.urv.deim.sob.entities.offers.OfferVO;
import java.util.List;

public class UserVO implements IValueObject{

    private final int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private List<OfferVO> reservas;
    private String password;
    
    public UserVO(int id, String firstName, String lastName, String email, String phone, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }
    
    public String getFirstName() {
        return fixNull(this.firstName);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return fixNull(this.lastName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return fixNull(this.email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return fixNull(this.phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<OfferVO> getReservas() {
        return reservas;
    }

    public void setReservas(List<OfferVO> reservas) {
        this.reservas = reservas;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String fixNull(String in) {
        return (in == null) ? "" : in;
    }

    public String getMessage() {

        return "\nFirst Name: " + getFirstName() + "\n"
                + "Last Name:  " + getLastName() + "\n"
                + "Email:      " + getEmail() + "\n"
                + "Phone:      " + getPhone() + "\n";
    }

    @Override
    public IKey getKey() {
        return new UserPk(this.id);    
    }

    public int getId() {
        return this.id;
    }
}
