/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.users;

import cat.urv.deim.sob.dao.IKey;

/**
 *
 * @author omauri
 */
public class UserPk implements IKey{
    	
    private int id;
	
    public UserPk(int id) {this.id = id;}

    public int getId() {return this.id;}

    public boolean equals(Object other) {
            return this.id == ((UserPk)other).getId();
    }
}
