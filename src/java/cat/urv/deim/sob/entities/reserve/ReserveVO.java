/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.reserve;
import cat.urv.deim.sob.dao.IKey;
import cat.urv.deim.sob.dao.IValueObject;
import cat.urv.deim.sob.entities.offers.*;

/**
 *
 * @author omauri
 */
public class ReserveVO extends OfferVO implements IValueObject{
    
    private final int id;
    private final int id_user;
    private int num;    // number of this offers booked by this customer

    public ReserveVO(int id, int num, int id_offer, String name, String shortDescription, String longDescription,
            int rooms, float costPerPeople, int id_user) {
        super(id_offer,  name, shortDescription, longDescription, rooms, costPerPeople);
        
        this.id = id;
        this.id_user = id_user;
        this.num = num;
    }
    
    public int getIdReserve(){
        return this.id;
    }
    
    public int getUserId() {
        return id_user;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    @Override
    public IKey getKey() {
        return new ReservePk(this.id);
    }
}
