/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.reserve;

import cat.urv.deim.sob.dao.*;
import cat.urv.deim.sob.entities.offers.*;
import cat.urv.deim.sob.entities.users.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author omauri
 */
public class ReserveDaoJavaBase implements IDao{
    
    private final String namedb;
    private static ReserveDaoJavaBase instance = null;
    
    public static ReserveDaoJavaBase getInstance(){
        if (ReserveDaoJavaBase.instance == null){
            ReserveDaoJavaBase.instance = new ReserveDaoJavaBase();
        }
        return ReserveDaoJavaBase.instance;
    }
    private ReserveDaoJavaBase() {
        this.namedb = "sobasedb";
    }
    
    @Override
    public IValueObject findByPrimaryKey(IKey key) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".reserves WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, ((ReservePk) key).getId());
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                int id_user = rs.getInt("id_user");
                int id_offer = rs.getInt("id_offer");
                int num_r = rs.getInt("num_reserves");
                // UserVO user = getUser(id_user);
                OfferVO offer = (OfferVO) getOffer(id_offer);
                return new ReserveVO (
                        // reserve:
                        rs.getInt("id"),
                        num_r,
                        // offer:
                        id_offer,
                        offer.getName(),
                        offer.getShortDescription(),
                        offer.getLongDescription(),
                        offer.getRooms(),
                        offer.getCostPerPeople(),
                        // user:
                        id_user);
            } else {
                return null;
                // TODO: Create a new one?
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * Not Used!
     * Only Admin Action
     * @return
     * @throws DaoException 
     */
    @Override
    public Collection findAll() throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * 
     * @param key
     * @return FILTER(reserves of a User(@id_user))
     * @throws DaoException 
     */
    public Collection findMyReserves(IKey key) throws DaoException {
        Collection retorn = new LinkedList();
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM " +namedb+".reserves WHERE id_user=?";
            ps = con.prepareStatement(sql);
            int id_user = ((UserPk) key).getId();
            int id_offer;
            int num_r;
            ps.setInt(1, id_user);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                id_offer = rs.getInt("id_offer");
                num_r = rs.getInt("num_reserves");
                // UserVO user = getUser(id_user);
                OfferVO offer = (OfferVO) getOffer(id_offer);
                retorn.add( (ReserveVO) new ReserveVO (
                        // reserve:
                        rs.getInt("id"),
                        num_r,
                        // offer:
                        id_offer,
                        offer.getName(),
                        offer.getShortDescription(),
                        offer.getLongDescription(),
                        offer.getRooms(),
                        (float) offer.getCostPerPeople(),
                        // user:
                        id_user));
            }
            return retorn;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    public void add(int id_user, int id_offer, int rooms) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        // TODO: Check if this offer with this user is in the list!
        try {
            con = createConnection();
            String sql = "INSERT INTO " +namedb+".reserves(id_user, id_offer, num_reserves) VALUES (?, ?, ?)";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id_user);
            ps.setInt(2, id_offer);
            ps.setInt(3, rooms);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    /**
     * Not Used!
     * Version 2.0 the User will delete and disable reserves
     * @param vo
     * @throws DaoException 
     */
    @Override
    public void update(IValueObject vo) throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Not Used in Version 1.0
     * Version 2.0 the User will delete and disable reserves
     * @param key
     * @throws DaoException 
     */
    @Override
    public void delete(IKey key) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "DELETE FROM " +namedb+".reserves WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, ((ReservePk) key).getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    private Connection createConnection() throws Exception {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection("jdbc:derby://localhost:1527/sobasedb", "oriol", "oriol");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    private IValueObject getUser(int id_user) throws DaoException {
        // TODO: Use Singletone Pattern for Daos
        UserDaoJavaBase dao = UserDaoJavaBase.getInstance();
        UserPk pk = new UserPk(id_user);
	UserVO user = (UserVO)dao.findByPrimaryKey(pk);
        return user;
    }
    
    private IValueObject getOffer(int id_offer) throws DaoException {
        // TODO: Use Singletone Pattern for Daos
        OfferDaoJavaBase dao = OfferDaoJavaBase.getInstance();
        OfferPk pk = new OfferPk(id_offer);
	OfferVO offer = (OfferVO)dao.findByPrimaryKey(pk);
        return offer;    
    }

    @Override
    public void add(IValueObject vo) throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
