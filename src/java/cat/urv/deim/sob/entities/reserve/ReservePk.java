/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.reserve;

import cat.urv.deim.sob.dao.IKey;

/**
 *
 * @author omauri
 */
public class ReservePk implements IKey{
    	
    private int id;

    public ReservePk(int id) {this.id = id;}

    public int getId() {return this.id;}

    public boolean equals(Object other) {
            return this.id ==((ReservePk)other).getId();
    }
}
