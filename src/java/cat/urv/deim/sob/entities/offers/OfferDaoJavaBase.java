/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.offers;

import cat.urv.deim.sob.dao.*;
import java.sql.*;
import java.util.Collection;
import java.util.*;
//TODO: Crear metodos que realmente necesite para la aplicación!!
/**
 *
 * @author omauri
 */
public class OfferDaoJavaBase implements IDao{

    final String namedb;
    private static OfferDaoJavaBase instance = null;
    
    public static OfferDaoJavaBase getInstance(){
        if (OfferDaoJavaBase.instance == null){
            OfferDaoJavaBase.instance = new OfferDaoJavaBase();
        }
        return OfferDaoJavaBase.instance;
    }
    
    private OfferDaoJavaBase() {
        this.namedb = "sobasedb";
    }
        @Override
    public IValueObject findByPrimaryKey(IKey key) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM "+namedb+".offers WHERE id = ?";   // SQLSyntaxErrorException
            ps = con.prepareStatement(sql);
            ps.setInt(1, ((OfferPk) key).getId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new OfferVO (
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("short_description"),
                        rs.getString("long_description"),
                        rs.getInt("rooms"),
                        rs.getFloat("cost_per_people"));
            } else {
                return null;
                // TODO: Create a new one?
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public Collection findAll() throws DaoException {
        Collection retorn = new LinkedList();
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "SELECT * FROM "+namedb+".offers";
            ps = con.prepareStatement(sql);     // SQLSyntaxErrorException
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                retorn.add(new OfferVO (rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("short_description"),
                        rs.getString("long_description"),
                        rs.getInt("rooms"),
                        rs.getFloat("cost_per_people")
                        ));
            }
            return retorn;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Not Used!
     * this should be only for Admin
     * @param vo
     * @throws DaoException 
     */
    @Override
    public void add(IValueObject vo) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "INSERT INTO" +namedb+".offers"
                    + "(name, short_description, long_description, rooms, cost_per_people)"
                    + " VALUES (?, ?, ?, ?, ?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, ((OfferVO) vo).getName());
            ps.setString(2, ((OfferVO) vo).getShortDescription());
            ps.setString(3, ((OfferVO) vo).getLongDescription());
            ps.setInt(4, ((OfferVO) vo).getRooms());
            ps.setFloat(5, ((OfferVO) vo).getCostPerPeople());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Not Used!
     * this should be only for Admin
     * @param vo
     * @throws DaoException 
     */
    @Override
    public void update(IValueObject vo) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "UPDATE " +namedb+".offers SET short_description=?,long_description=?,rooms=?,votes=?,stars=?, name=? WHERE id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, ((OfferVO) vo).getShortDescription());
            ps.setString(2, ((OfferVO) vo).getLongDescription());
            ps.setInt(3, ((OfferVO) vo).getRooms());
            ps.setInt(4, ((OfferVO) vo).getVotes());
            ps.setInt(5, ((OfferVO) vo).getStars());
            ps.setString(6, ((OfferVO) vo).getName());
            ps.setInt(7, ((OfferVO) vo).getIdOffer());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    /**
     * Not Used!
     * this should be only for Admin
     * @param key
     * @throws DaoException 
     */
    @Override
    public void delete(IKey key) throws DaoException {
        PreparedStatement ps = null;
        Connection con = null;
        try {
            con = createConnection();
            String sql = "DELETE FROM " +namedb+".offers WHERE id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, ((OfferPk) key).getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
            }
        }
    }
    
    private Connection createConnection() throws Exception {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            return DriverManager.getConnection("jdbc:derby://localhost:1527/sobasedb", "oriol", "oriol");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
