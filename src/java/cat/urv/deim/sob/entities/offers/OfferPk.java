/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.offers;

import cat.urv.deim.sob.dao.IKey;

/**
 *
 * @author omauri
 */
public class OfferPk implements IKey{
    	
    private int id;

    public OfferPk(int id) {this.id = id;}

    public int getId() {return this.id;}

    public boolean equals(Object other) {
            return this.id == ((OfferPk)other).getId();
    }
}
