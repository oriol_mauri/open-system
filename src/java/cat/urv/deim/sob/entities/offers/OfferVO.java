/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.entities.offers;
import cat.urv.deim.sob.dao.IKey;
import cat.urv.deim.sob.dao.IValueObject;
import java.util.*;

/**
 *
 * @author omauri
 */
public class OfferVO implements IValueObject{
    
    private final int id;
    private String name;
    private String shortDescription;
    private String longDescription;
    private int rooms;
    private float costPerPeople;
    //private Country conutry;
    //private List<String> tags;
    private int stars;
    private int votes;
    private Date date;

    public OfferVO(int id, String name, String shortDescription, String longDescription, int rooms, float costPerPeople) {
        //this.tags = new ArrayList<>();
        this.id = id;
        this.name = name;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.rooms = rooms;
        this.costPerPeople = costPerPeople;
        // this.conutry = conutry.charAt(country);
        this.stars = 0;
        this.votes = 0;
    }
    
    
    /**
    *   @description Reserve a 'v' vacances for the offer
    *   @param v int;
    *   @return: boolean to see if the operation is allowed or not
    */
    public boolean applayRooms(int v){
        if (this.rooms >= v){
            this.rooms = this.rooms -v;
            return true;
        }
        return false;
    }
    /*
        GETS and SETTERS:
    */

    public int getIdOffer() {
        return id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int vacancies) {
        this.rooms = vacancies;
    }

    public float getCostPerPeople() {
        return costPerPeople;
    }

    public void setCostPerPeople(float costPerPeople) {
        this.costPerPeople = costPerPeople;
    }
    /*
    public Country getConutry() {
        return conutry;
    }

    public void setConutry(Country conutry) {
        this.conutry = conutry;
    }*/
    /*
     *TODO in the jsp:
     *  List<String> list = s.print(); 
     *  <c:forEach items="${list}" var="item">
            ${item}<br>
        </c:forEach>
    
    public List<String> getTags() {
        return tags;
    }
 */
    public int getStars() {
        return stars;
    }

    public void Vote(int stars) {
        this.addVote();
        this.stars = (this.stars + stars) / this.votes;
    }

    public int getVotes() {
        return votes;
    }

    public void addVote() {
        this.votes ++;
    }
    
    /**
     * @param tag
     * add a tag to the list
     */
    /*
    public void addTag(String tag) {
        this.tags.add(tag);
    }
    */
    /**
     *  @param t
     *  @return boolean
     *  returns if the Offer contains an especific tag
     */
    /*
    public boolean containsTag(String t){
       try{
           return this.tags.contains(t);
       }catch(NullPointerException e){
           return false;
       }
    }
*/
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    /*
        FixNull
        convert a null string in a valid string
    */
    private String fixNull(String in) {
        return (in == null) ? "" : in;
    }
    /*
        GET Short and Long Message for the List or for the detail view
    */
    public String getShortMessage() {

        return "\nOffer Name: " + getName()+ "\n"
                + getShortDescription()+ "\n";
    }
    
    public String getMessage() {

        return "\nOffer Name: " + getName()+ "\n"
                + getLongDescription()+ "\n"
                +"Vacancies: " + getRooms()+ "\n"
                +"Cost/People: " + getCostPerPeople()+ "€/people\n";
    }

    @Override
    public IKey getKey() {
        return new OfferPk(this.id);
    }
}
