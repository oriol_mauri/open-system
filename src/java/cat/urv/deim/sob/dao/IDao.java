/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.dao;

/**
 *
 * @author omauri
 */
import java.util.Collection;

public interface IDao {
        // TODO: Use Singletone Pattern for Daos
	public IValueObject findByPrimaryKey(IKey key) throws DaoException;
	public Collection findAll() throws DaoException;
	public void add(IValueObject vo) throws DaoException;
	public void update(IValueObject vo) throws DaoException;
	public void delete(IKey key) throws DaoException;
	
}
