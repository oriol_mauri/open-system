/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.dao;

/**
 *
 * @author omauri
 */
public interface IKey {

    /**
     *
     * @return String
     */
    @Override
	public String toString();

    /**
     *
     * @param other
     * @return boolean
     */
    @Override
	public boolean equals(Object other);
	
}
