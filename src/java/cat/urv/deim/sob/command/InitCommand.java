package cat.urv.deim.sob.command;

import cat.urv.deim.sob.entities.users.UserVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

public class InitCommand implements Command {

    @Override
    public void execute(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        // 1. process the request
        //TODO: Offer connect with Dao and DB
        // 2. produce the view with the web result
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);
        /*
        ServletContext context = request.getSession().getServletContext();
        context.getRequestDispatcher("index.jsp").forward(request, response);*/
    }
}
