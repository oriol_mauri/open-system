/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.user;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.DaoException;
import cat.urv.deim.sob.dao.IDao;
import cat.urv.deim.sob.dao.IKey;
import cat.urv.deim.sob.entities.users.*;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author omauri
 */
public class Profile implements Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        ServletContext context = request.getServletContext();
        IDao user_dao = (UserDaoJavaBase) context.getAttribute("user_dao");
         
        UserDaoJavaBase dao = UserDaoJavaBase.getInstance();
        UserVO user = (UserVO) session.getAttribute("user");
        // el usuari no esta loggejat:
        if (user == null){
            response.sendRedirect("login.do");
        }
        
        if (request.getAttribute("action").equals("update_user.do")){
            try {
                user = new UserVO(0, request.getParameter("first_name"), request.getParameter("last_name"), request.getParameter("email"), request.getParameter("phone"), request.getParameter("password"));
                user_dao.update(user);
                session.setAttribute("user", user);
                response.sendRedirect("profile.do");
            } catch (DaoException ex) {
                RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);            
            }
        }
        
        if (request.getAttribute("action").equals("logout_submit.do")){
            session.setAttribute("user", null);
            response.sendRedirect("init.do");
        }
        
        if (request.getAttribute("action").equals("delete_user.do")){
            IKey key = user.getKey();
            try {
                dao.delete(key);
                session.setAttribute("user", null);
                response.sendRedirect("login.do");
            } catch (DaoException ex) {
                RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);            
            }
        }
        
        // default
        if (request.getAttribute("action").equals("profile.do")){
            RequestDispatcher view = request.getRequestDispatcher("views/user/profile.jsp");
            view.forward(request, response);
        }
    }
    
}
