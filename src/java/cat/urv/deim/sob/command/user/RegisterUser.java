/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.user;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.*;
import cat.urv.deim.sob.entities.users.*;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author omauri
 */
public class RegisterUser implements Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                HttpSession session = request.getSession();
        // UserDaoJavaBase dao = session.getAttribute("user_dao");
        ServletContext context = request.getServletContext();
        UserDaoJavaBase user_dao = (UserDaoJavaBase) context.getAttribute("user_dao");
        IValueObject user = (UserVO) session.getAttribute("user");
        
        // el usuari ja esta loggejat i no hem submès el button de register:
        if (user != null){
            response.sendRedirect("profile");
        }
        
        // entrem fent click al botó de la vista
        if(request.getAttribute("action").equals("register_submit.do")){
            try {
                user = new UserVO(0, request.getParameter("first_name"), request.getParameter("last_name"), request.getParameter("email"), request.getParameter("phone"), request.getParameter("password"));
                user_dao.add(user);
                user = (UserVO) user_dao.findByEmail(request.getParameter("email"));
                session.setAttribute("user", user);
                if(session.getAttribute("last_action") != null){
                    response.sendRedirect((String) session.getAttribute("last_action"));
                    session.setAttribute("last_action", null);
                }else{
                    response.sendRedirect("profile.do");
                }
            } catch (DaoException ex) {
                RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);
            }
        }
        
        if(request.getAttribute("action").equals("register.do") && user == null){
            RequestDispatcher view = request.getRequestDispatcher("views/user/register.jsp");
            view.forward(request, response);   
        }
    }
    
}
