/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.user;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.DaoException;
import cat.urv.deim.sob.entities.users.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author omauri
 */
public class Login implements Command{
    
    @Override
    public void execute(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext context = request.getServletContext();
        
        HttpSession session = request.getSession();
        // UserDaoJavaBase dao = session.getAttribute("user_dao");
        UserDaoJavaBase dao = (UserDaoJavaBase) context.getAttribute("user_dao");
        
        UserVO user = (UserVO) session.getAttribute("user");
        PrintWriter out = response.getWriter();
        RequestDispatcher view;
        // el usuari ja esta loggejat:
        if (user != null){
            session.setAttribute("user", user);
            response.sendRedirect("profile.do");
        }
        // si no està logejat
        // si em entrat amb l'acció de login_submit:
        if (request.getAttribute("action").equals("login_submit.do")){

            String password = request.getParameter("password");
            String email = request.getParameter("email");
            try { // mirar a la base de dades si existeix

                switch(dao.VerifyPassword(email, password)){
                    case 0: // contrasenya correcta
                        user = (UserVO) dao.findByEmail(email);
                        session.setAttribute("user", user);
                        if(session.getAttribute("last_action") != null){
                            response.sendRedirect((String) session.getAttribute("last_action"));
                            session.setAttribute("last_action", null);
                        }else{
                            response.sendRedirect("profile.do");
                        }
                        break;
                    case 1:
                        session.setAttribute("user", null);
                        out.println("Incorrect password");
                        response.sendRedirect("login.do");
                        break;
                    case 2: // usuari no existeix
                        session.setAttribute("user", null);
                        request.setAttribute("email", email);
                        view = request.getRequestDispatcher("views/user/register.jsp");
                        view.forward(request, response);
                        break;
                }

            } catch (DaoException | NullPointerException ex) {
                view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);
            }            
        }

        if (request.getAttribute("action").equals("logout_submit.do")){
            session.setAttribute("user", null);
            response.sendRedirect("init.do");
        }
        // entres per primer cop
        if (request.getAttribute("action").equals("login.do")){
            request.getRequestDispatcher("/views/user/login.jsp").forward(request, response);
        }
    }
}
