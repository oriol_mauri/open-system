/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.offer;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.*;
import cat.urv.deim.sob.entities.offers.*;
import cat.urv.deim.sob.entities.reserve.ReserveDaoJavaBase;
import cat.urv.deim.sob.entities.users.UserVO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 *
 * @author omauri
 */
public class DetailsOffer implements Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        ServletContext context = request.getServletContext();
        ReserveDaoJavaBase reserve_dao = (ReserveDaoJavaBase) context.getAttribute("reserve_dao");
        IDao offer_dao = (OfferDaoJavaBase) context.getAttribute("offer_dao");
        try {
            if (request.getAttribute("action").equals("reserve_offer.do")){
                
                if (session.getAttribute("user") == null){
                    session.setAttribute("last_action", "details_offer.do?id_offer="+request.getParameter("id_offer"));
                    response.sendRedirect("login.do");
                }else{
                    int rooms = -1;
                    int places = -1;
                    int id_offer = Integer.parseInt(request.getParameter("id_offer"));
                    
                    IKey key = new OfferPk(id_offer);
                    OfferVO offer = (OfferVO) offer_dao.findByPrimaryKey(key);
                    
                    try{
                        rooms = Integer.parseInt(request.getParameter("rooms"));
                        // update offer
                        places = offer.getRooms()-rooms;
                    }catch(Exception e){
                        rooms = -1;
                    }
                    if(places < 0 || ( rooms <= 0 )){
                        // error de reserva
                        response.sendRedirect("details_offer.do?id_offer="+offer.getIdOffer());
                    }else{
                        offer.setRooms(places);
                        offer_dao.update(offer);
                        int id_user = ((UserVO) session.getAttribute("user")).getId();
                        reserve_dao.add(id_user, id_offer, rooms);

                        response.sendRedirect("list_reserves.do");
                    }                    
                }
            }else{

                String id_s = request.getParameter("id_offer");
                int id = Integer.parseInt(id_s);

                OfferPk pk = new OfferPk(id);

                OfferVO offer = (OfferVO) offer_dao.findByPrimaryKey(pk);

                request.setAttribute("offer", offer);
                // view details
                RequestDispatcher view = request.getRequestDispatcher("/views/offers/details.jsp");
                view.forward(request, response);
            }
            
        } catch (DaoException | NumberFormatException | NullPointerException ex) {
                response.sendRedirect("list_offers.do");
        }    
    }
    
}
