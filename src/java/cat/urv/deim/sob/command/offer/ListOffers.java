/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.offer;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.DaoException;
import cat.urv.deim.sob.dao.IDao;
import cat.urv.deim.sob.entities.offers.OfferDaoJavaBase;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author omauri
 */
public class ListOffers implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //dao in session?:
        ServletContext context = request.getServletContext();
        IDao offer_dao = (OfferDaoJavaBase) context.getAttribute("offer_dao");
                
        try {
            Collection c = offer_dao.findAll();
            Iterator it = c.iterator();
            request.setAttribute("offer_list", it);
            if (it == null){
                RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);
            }
            //default and unique
            RequestDispatcher view = request.getRequestDispatcher("views/offers/list.jsp");
            view.forward(request, response);
        
        } catch (DaoException ex) {
            RequestDispatcher view = request.getRequestDispatcher("error.jsp");
            view.forward(request, response);
        }
    }
    
}
