/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.reserve;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.DaoException;
import cat.urv.deim.sob.entities.offers.*;
import cat.urv.deim.sob.entities.reserve.*;
import cat.urv.deim.sob.entities.users.UserVO;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author omauri
 */
public class DetailsReserve implements Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        ServletContext context = request.getServletContext();
        ReserveDaoJavaBase reserve_dao = (ReserveDaoJavaBase) context.getAttribute("reserve_dao");
        OfferDaoJavaBase offer_dao = (OfferDaoJavaBase) context.getAttribute("offer_dao");
        UserVO user = (UserVO) session.getAttribute("user");
        // el usuari no esta loggejat:
        if (user == null){
            RequestDispatcher view = request.getRequestDispatcher("views/user/login.jsp");
            view.forward(request, response);
        }
        try {
            int id = Integer.parseInt(request.getParameter("id_reserve"));
            ReservePk pk = new ReservePk(id);
            ReserveVO reserve = (ReserveVO) reserve_dao.findByPrimaryKey(pk);
            OfferPk offer_pk = new OfferPk(reserve.getIdOffer());
            OfferVO offer = (OfferVO)  offer_dao.findByPrimaryKey(offer_pk);
            request.setAttribute("reserve", reserve);
            
            // remove reserve
            if (request.getAttribute("action").equals("delete_reserve.do")){
                
                offer.setRooms(offer.getRooms() + reserve.getNum());
                offer_dao.update(offer);
                reserve_dao.delete(reserve.getKey());
                response.sendRedirect("list_reserves.do");
            }
            else{
                if (reserve == null){
                    RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                    view.forward(request, response);
                }
                // view details
                RequestDispatcher view = request.getRequestDispatcher("/views/reserves/details.jsp");
                view.forward(request, response);
            }
        } catch (DaoException ex) {
                RequestDispatcher view = request.getRequestDispatcher("/views/reserves/list.jsp");
                view.forward(request, response);        
        }
    }   
}
