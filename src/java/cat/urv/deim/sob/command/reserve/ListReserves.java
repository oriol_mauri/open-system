/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.urv.deim.sob.command.reserve;

import cat.urv.deim.sob.command.Command;
import cat.urv.deim.sob.dao.*;
import cat.urv.deim.sob.entities.reserve.ReserveDaoJavaBase;
import cat.urv.deim.sob.entities.users.*;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author omauri
 */
public class ListReserves implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletContext context = request.getServletContext();
        ReserveDaoJavaBase reserve_dao = (ReserveDaoJavaBase) context.getAttribute("reserve_dao");
        HttpSession session = request.getSession();
        UserVO user = (UserVO) session.getAttribute("user");
        
        if (user == null){
            RequestDispatcher view = request.getRequestDispatcher("views/user/login.jsp");
            view.forward(request, response);
        }
        UserPk userpk = new UserPk(user.getId());
        try {
            Collection c = reserve_dao.findMyReserves(userpk);
            Iterator it = c.iterator();
            request.setAttribute("reserves_list", it);
            if (it == null){
                RequestDispatcher view = request.getRequestDispatcher("error.jsp");
                view.forward(request, response);
            }
                        
            RequestDispatcher view = request.getRequestDispatcher("views/reserves/list.jsp");
            view.forward(request, response);
        
        } catch (DaoException ex) {
            RequestDispatcher view = request.getRequestDispatcher("error.jsp");
            view.forward(request, response);
        }    
    }
    
}
