package cat.urv.deim.sob.command;

import cat.urv.deim.sob.command.user.*;
import cat.urv.deim.sob.command.reserve.*;
import cat.urv.deim.sob.command.offer.*;
import cat.urv.deim.sob.dao.*;
import cat.urv.deim.sob.entities.offers.OfferDaoJavaBase;
import cat.urv.deim.sob.entities.reserve.ReserveDaoJavaBase;
import cat.urv.deim.sob.entities.users.UserDaoJavaBase;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Map;
import java.util.HashMap;
import java.util.StringTokenizer;

public class ControllerServlet extends HttpServlet {

    private Map commands = new HashMap();

    @Override
    public void init() {
    // list of commands
        this.commands.put("init.do", new InitCommand());
        // users
        this.commands.put("login.do", new Login());
        this.commands.put("login_submit.do", new Login());             // ?
        
        this.commands.put("profile.do", new Profile());
        this.commands.put("delete_user.do", new Profile());            // ?
        this.commands.put("update_user.do", new Profile());            // ?
        this.commands.put("logout_submit.do", new Profile());            // ?
        
        this.commands.put("register.do", new RegisterUser());
        this.commands.put("register_submit.do", new RegisterUser());   // ?
        // offers
        this.commands.put("list_offers.do", new ListOffers());
        this.commands.put("details_offer.do", new DetailsOffer());
        this.commands.put("reserve_offer.do", new DetailsOffer());
        // reserves
        this.commands.put("list_reserves.do", new ListReserves());
        this.commands.put("details_reserve.do", new DetailsReserve());
        this.commands.put("delete_reserve.do", new DetailsReserve());  // ?
        
        ServletContext context = getServletContext();
        
        IDao offer_dao = OfferDaoJavaBase.getInstance();
        context.setAttribute("offer_dao", offer_dao);
        IDao reserve_dao = ReserveDaoJavaBase.getInstance();
        context.setAttribute("reserve_dao", reserve_dao);
        UserDaoJavaBase user_dao = UserDaoJavaBase.getInstance();
        context.setAttribute("user_dao", user_dao);
    }

    protected void processCommand(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        // 1. choose action
        String uri = request.getRequestURI();
        
        StringTokenizer token = new StringTokenizer(uri, "/");
        String action = "init.do";
        
        while(token.hasMoreTokens()){
            action = token.nextToken();
        }
        // TODO: Read ACTION

        Command command = (Command) commands.get(action);
        
        if (null == command) {
            throw new IllegalArgumentException(
                    "No command for the action: " + action);
        }
        // 3. run the command
        request.setAttribute("action", action);
        command.execute(request, response);
    }

    @Override
    public void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        processCommand(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        processCommand(request, response);
    }
}
